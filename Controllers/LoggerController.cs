﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCLogger.Data;
using SiCLogger.Models;
using SiCLogger.Services;

namespace SiCLogger
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoggerController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public LoggerController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Logger
        [HttpGet]
        public IEnumerable<Logger> GetLogger()
        {


            return _context.Logger;
        }


        [HttpGet("nocrypt/{key}")]
        public IEnumerable<Logger> GetLoggerDCrypt([FromRoute] string key)
        {


            if( key != "1313ab")
            {


                Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                return null;
            }


            

            List<Logger> logs = new List<Logger>();


            foreach (Logger l in _context.Logger)
            {
                try
                {
                    l.message = Crypto.DecryptStringAES(l.message, "AAA");

                    Console.WriteLine(l.message);

                }
                catch (Exception e)
                {

                    l.message = "invalid";

                }
                logs.Add(l);


            }



            return logs;
           

        }



        // POST: api/Logger
        [HttpPost]
        public async Task<IActionResult> PostLogger([FromBody] Logger logger)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            DateTime now = DateTime.Now;


            logger.Date = now.Date + "-" + now.Hour + ":" + now.Minute + ":" + now.Second;

            logger.message = Crypto.EncryptStringAES(logger.message, "AAA");

            Console.WriteLine(logger.message);

            _context.Logger.Add(logger);
            await _context.SaveChangesAsync();



            return CreatedAtAction("GetLogger", new { id = logger.ID }, logger);
        }



        private bool LoggerExists(int id)
        {
            return _context.Logger.Any(e => e.ID == id);
        }
    }
}